﻿using System;
using System.Windows.Forms;
using HtmlAgilityPack;

namespace OneStripper
{
    public partial class Form1 : Form
    {
        private string _clipBoard;
        private HtmlAgilityPack.HtmlDocument _htmlDocument;

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            _clipBoard = Clipboard.GetText(TextDataFormat.Html);

            if (string.IsNullOrEmpty(_clipBoard))
            {
                MessageBox.Show(@"Clipboard is empty or HTML is invalid");
                return;
            }

            _htmlDocument = new HtmlAgilityPack.HtmlDocument();

            richTextBox1.Text = _clipBoard;

            if (_clipBoard.Contains("<!--StartFragment-->"))
            {
                string[] startF = {"<!--StartFragment-->"};
                string topTable = _clipBoard.Split(startF, StringSplitOptions.None)[1];

                string[] endF = {"<!--EndFragment-->"};
                _clipBoard = topTable.Split(endF, StringSplitOptions.None)[0];
            }

            _htmlDocument.LoadHtml(_clipBoard);

            if (_clipBoard.Contains("table"))
            {
                foreach (HtmlNode element in _htmlDocument.DocumentNode.SelectNodes("//table"))
                {
                    element.Attributes.RemoveAll();
                    element.Attributes.Add("border", "1");
                    element.Attributes.Add("style", "border: 1pt solid black; border-spacing: 0px;");
                }
            }

            foreach (var element in _htmlDocument.DocumentNode.SelectNodes("//p | //span"))
            {
                for (var i = 0; i < element.Attributes.Count; i++)
                {
                    if (element.Attributes[i].Name == "style")
                    {
                        if (element.Attributes[i].Value.Contains("bold"))
                        {
                            element.Attributes[i].Remove();
                            element.Attributes.Add("style", "font-weight:bold");
                        }
                        else
                        {
                            element.Attributes[i].Remove();
                        }
                    }
                    else
                    {
                        element.Attributes[i].Remove();
                    }
                }
            }

            foreach (var element in _htmlDocument.DocumentNode.SelectNodes("//td | //tr | //ul | //li | //ol"))
            {
                element.Attributes.RemoveAll();
            }

            richTextBox2.Text = _htmlDocument.DocumentNode.OuterHtml;
            Clipboard.SetText(_htmlDocument.DocumentNode.OuterHtml);
           }
    }
}
